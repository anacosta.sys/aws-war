FROM tomcat:8.5-alpine

EXPOSE 8080

RUN rm -rf /usr/local/tomcat/webapps/*

COPY ./target/example.war /usr/local/tomcat/webapps/ROOT.war

CMD [ "catalina.sh", "run" ]