#!/usr/bin/env bash

# Compila o projeto
mvn clean package

# Cria a imagem docker
docker image build -t webapps .

# Executa a aplicação em uma docker na porta 8080
docker run -it --rm -p 8080:8080 webapps

